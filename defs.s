; all of these are 3-byte addresses -- the first byte is the RAM bank and the second
; they point to a 3-byte car address and 1-byte car type
; and a 3-byte cdr address and 1-byte cdr type
; totalling at 8 bytes
.define free_list $2 ; address of the first element of the free list
.define program $5 ; address of the program
.define stack $8 ; a list of program states by calls
.define symbols $11 ; a list of symbols

; various allocation stuff in low memory
.define str_free $14

; some state stuff
.define irq_vector $41 ; two bytes

; some scratch bytes
.define sa $78
.define sa_low $78
.define sa_high $79
; mparam0 through mparam2 holds any far pointers
.define mparam0 $7a
.define mparam1 $7b
.define mparam2 $7c
.define mparam3 $7d
.define zeromem $7e
.define scratch $7f
