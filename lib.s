; lib.s
; various internal functions

chrcode:; converts PETSCII to VERA character code
	cmp #$60
	bcc @neor1
	eor #$20
	bra @done
@neor1:	cmp #$40
	bcc @done
	and #$3f
@done:	and #$7f
	rts

vpoke:	; writes A to X:Y on the VRAM
	stx $9f20
	sty $9f21
	sta $9f23
	rts

clear:	pha
	lda #$93
	jsr $ffd2
	pla
	rts

vpeek:	; reads A from X:Y on the VRAM
	stx $9f20
	sty $9f21
	lda $9f23
	rts

chrout:	; writes accumulator to the screen with wrapping and scrolling
	pha
	pha
	jsr blink
	pla
	jsr $ffd2
	jsr blink
	pla
	rts

chrin:	; reads a character to the accumulator
	jsr $ff6e
	.addr $ffe4
	.byt 0
	and #$ff
	beq chrin ; no bytes in the buffer
	rts

blink:	; blink the current cursor position
	sec
	jsr $fff0
	stx scratch
	tya
	asl a
	ina
	tax
	ldy scratch
	jsr vpeek
	cmp #$10
	bcs @off
	lda #$d0
	jmp vpoke
@off:	lda #$0d
	jmp vpoke

strout:	; writes null terminated string at sa to the screen
	lda (sa)
	beq @done
	jsr chrout
	inc sa_low
	bne @sanc
	inc sa_high
@sanc:	bra strout
@done:	rts
