; initialisation
; alo includes the other files

.include "defs.s"

	; address stuff
	.org  $7ff
	.addr $801,$80c,$5,$8dce,$3331,$302c,$1600,$a08
	.addr $9e00,$3824,$3931,0,0

	; setup the free list
	stz 0 ; set the high memory to the first bank
	stz scratch ; store the bark in scratch
	stz zeromem ; 0 is useful to have in the 0 page
	ldx #0 ; address low byte
	ldy #$a0 ; address high byte
	stx free_list
	sty free_list + 1
	lda #$c0
	sta mparam2
	sta mparam0
	lda #8
	sta mparam1
setup_free_list:
	jsr make_free_list
	; increment the bank
	ldy #$a0 ; reset to the start of the bank
	tya
	sta (sa)
	dec sa_low
	dec sa_low
	lda scratch
	ina
	sta scratch
	sta (sa)
	bcc setup_free_list
	inc sa_low
	inc sa_low
	lda #0
	sta (sa)

	; setup the string free list
	; starts at $4000
	ldx #0
	stx mparam2
	stx mparam1
	inc mparam1
	lda #$9f
	sta mparam0
	ldy #$40
	jsr make_free_list

	; setup the screen
	lda #2
	jsr $ff5f
	jsr $ffcc
	jsr clear
	lda #<welcome
	sta sa_low
	lda #>welcome
	sta sa_high
	jsr strout

@echo:	jsr chrin
	jsr chrout
	bra @echo

	rts

.include "lib.s"

; various strings
welcome:.byt "8LISP BY J RAIN DE JAGER", $0d, 0

isr:	pha
	phx
	phy
	php
	jsr blink
	plp
	ply
	plx
	pla
	jmp (irq_vector)

make_free_list:
	; a function to make a free list
	; starts at low byte x, high byte y
	; ends at high byte mparam0
	; in steps of mparam1
	; if mparam2 isn't zero, it sets a long pointer
	sty sa_high
	txa
	sta sa_low
	adc mparam1
	tax
	bcc @noxc
	iny
@noxc:	lda mparam2
	beq @near ; only setup near pointers
	lda scratch
	sta (sa)
	inc sa_low
@near:	txa
	sta (sa)
	inc sa_low
	tya
	sta (sa)
	cpy mparam0
	bcc make_free_list
	rts
