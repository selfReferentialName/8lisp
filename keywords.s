keywords:
	; stored as string, 0, address
	; the order here corresponds to the order in r7rs
	; most keywords have an abbreviated form
	; some keywords are left out
	; this includes delayed evaluation and prameterization

	; equivalence predicates

	.byt "EQV?", 0
	.addr std_eqv
	; eq? is similar enough that I don't care
	.byt "EQ?", 0
	.addr std_eqv

	.byt "EQUAL?", 0
	.addr std_equal

	; numbers

	.byt "NUMBER?", 0
	.addr std_number
	.byt "NUM?", 0
	.addr std_number

	.byt "REAL?", 0
	.addr std_real

	; no complex and rational numbers
	.byt "INTEGER?", 0
	.addr std_integer
	.byt "INT?", 0
	.addr std_integer

	.byt "EXACT?", 0
	.addr std_integer ; only integers are exact
	.byt "INEXACT?", 0
	.addr std_real ; only reals are inexact
	.byt "EXACT-INTEGER?", 0
	.addr std_integer ; no support for inexact integers

	.byt "FINITE?", 0
	.addr std_finite
	.byt "FIN?", 0
	.addr std_finite

	.byt "INFINITE?", 0
	.addr std_infinite
	.byt "INF?", 0
	.addr std_infinite

	.byt "NAN?", 0
	.addr std_nan

	.byt "=", 0
	.addr std_equ

	.byt "<", 0
	.addr std_lt

	.byt ">", 0
	.addr std_gt

	.byt "<=", 0
	.addr std_lte

	.byt ">=", 0
	.addr std_gte

	.byt "ZERO?"
	.addr std_zero

	.byt "POSITIVE?"
	.addr std_positive
	.byt "+?"
	.addr std_positive

	.byt "NEGATIVE?"
	.addr std_negative
	.byt "-?"
	.addr std_negative

	.byt "ODD?"
	.addr std_odd

	.byt "EVEN?"
	.addr std_even

	.byt "MAX"
	.addr std_max

	.byt "MIN"
	.addr std_min

	.byt "+", 0
	.addr std_add

	.byt "*", 0
	.addr std_mul

	.byt "-", 0
	.addr std_sub

	.byt "/", 0
	.addr std_div

	.byt "ABS", 0
	.addr std_abs

	; some divide and round procedures have been cut
	.byt "QUOTIENT", 0
	.addr std_quotient
	.byt "QOT", 0
	.addr std_quotient

	.byt "REMAINDER", 0
	.addr std_modulo
	.byt "MODULO", 0
	.addr std_modulo
	.byt "MOD", 0
	.addr std_modulo

	; gcd and lcm are easier to implement in lisp
	; numerator, denominator, and rationalize aren't supported
	; all these rounnding functions are exact
	.byt "FLOOR"
	.addr std_floor
	.byt "CIELING"
	.addr std_cieling
	.byt "CIEL"
	.addr std_cieling
	.byt "TRUNCATE"
	.addr std_truncate
	.byt "TRUNC"
	.addr std_truncate
	.byt "ROUND"
	.addr std_round

	; float math functions are TODO
	.byt "SQUARE"
	.addr std_square
	.byt "SQ"
	.addr std_square

	; SQRT and EXACT-INTEGER-SQRT are TODO
	.byt "EXPT"
	.addr std_expt
	.byt "^" ; up arrow key
	.addr std_expt

	; complex math functions are not supported
	; nor is exactness
	.byt "NUMBER->STRING"
	.addr std_number_string
	.byt "NUM->STR"
	.addr std_number_string

	.byt "STRING->NUMBER"
	.addr std_string_number
	.byt "STR->NUM"
	.addr std_string_number

	; booleans

	.byt "NOT"
	.addr std_not

	.byt "BOOLEAN?"
	.addr std_boolean
	.byt "BOOL?"
	.addr std_boolean

	.byt "BOOLEAN="
	.addr std_bool_eq
	.byt "BOOL="
	.addr std_bool_eq
